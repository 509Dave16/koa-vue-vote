# README #

### What is this repository for? ###

* This web app is a re-interpretation of Marcus Hammarberg's Koa Vote App that was used as a learning tool in his "Introduction to Koa Javascript" pluralsight course. My version is different in that it is a Single Page App by using VueJS along with the the Waterline Orm instead of using SwigJS and MonkDB. This purely a demonstration/learning project. It is **not complete** yet.

### How do I get set up? ###

- **Summary of set up**
    * Install node by following the instructions here(I used the latest LTS version): https://nodejs.org/en/download/
    * Right now this project is only setup for running in a Development environment.
    * Packages will need to be installed for the Back-end Api Server(.) and the Front-end File Server(./public).
    * And both servers will need to be running for the application to function as a whole.
- **Setup Back-end(in root folder)**
``` bash
# 1) Install dependencies.
npm install

# 2) Configure DB. Please see Database configuration section for information on configuring the db connection.

# 3) Run App
node app.js
```
- **Setup Front-end(in ./public folder)**
``` bash
# 1) Install dependencies.
npm install

# 2) Serve with hot reload at localhost:8080.
npm run dev
```
- **Database configuration**
    * The application is currently hard coded to use a MySql adapter and local connection at **./db/waterline-injection.js**.
    * In order for the application to work in your environment, you will either need to setup a MySql database with the same name, user credentials, and port. Or change the configuration to use your own custom MySql database.
- **Run Back-end tests(not fully complete)**
```bash
# 1) Install mocha globally.
npm install mocha -g

# 2) Run mocha in root folder(.)
mocha
```
- **Run Front-end tests(not fully complete)**
```bash
# 1) Run the following command
npm run test
```

### Who do I talk to? ###
* Repo owner or admin

### TODO ###
- Setup the Koa Server to have a 'prod' option that will cause it to use the compiled/minified CSS and JS files from the front-end.
- Setup the VueJs front end to either check if the server is running on prod or have it compiled so that http://localhost:3000 is **not** used when the Js is compiled for 'prod'.
- Genericize the testing of the Back-end resource Apis, so that testing can be finished for the Back-end.
- Add testing for VueJS Front-end.
- Update VueJS pre-transition check events.