var GenericApi = require('./../generic-api.js');
//todo Eventually allow for the overriding
//of action logic, and the provision of before/after logic
var UserApiRouter = GenericApi('users');
module.exports = UserApiRouter;