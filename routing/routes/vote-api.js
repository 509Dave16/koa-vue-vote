var GenericApi = require('./../generic-api.js');
//todo Eventually allow for the overriding
//of action logic, and the provision of before/after logic
var VoteApiRouter = GenericApi('votes');
module.exports = VoteApiRouter;