var requireDir = require('require-dir');
var routers = requireDir('./routes');

//Attaches router's routes to the Koa Middlewar
module.exports = function(app) {
    var routerNames = Object.keys(routers);
    for(var index in routerNames) {
        var routerName = routerNames[index];
        var router = routers[routerName];
        app
            .use(router.routes())
            .use(router.allowedMethods()); 
    }
};
