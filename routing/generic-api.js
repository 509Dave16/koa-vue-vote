var router = require("koa-router")();

function GenericApi(resourceName) {
    var baseUrl = '/' + resourceName;
    var singleResourceUrl = baseUrl + '/:id';
    var multiResourceUrl = baseUrl + '/';
    
    router.post(baseUrl, function *(next){
        var resourceFromRequest = this.request.body;
        var resourceCreated = yield this._waterline.collections[resourceName].create(resourceFromRequest);
        this.response.body = resourceCreated;
    });
    
    router.get(multiResourceUrl, function *(next){
       var resources = yield this._waterline.collections[resourceName].find({});
       this.response.body = resources; 
    });

    router.get(singleResourceUrl, function *(next){
        var resourceId = this.params.id;
        var resource = yield this._waterline.collections[resourceName].findOne({
            id : resourceId
        });
        if(!resource) {
          resourceNotFoundError(this); 
        }
        this.response.body = resource;
    });

    router.put(singleResourceUrl, function *(next){
        var resourceId = this.params.id;
        var resourceFromRequest = this.request.body;
        var resources = yield this._waterline.collections[resourceName].update(
            { id : resourceId },
            resourceFromRequest        
        );
        if(!resources.length) {
          resourceNotFoundError(this); 
        }
        this.response.status = 204;
    });

    router.del(singleResourceUrl, function *(){
        var resourceId = this.params.id;
        var resources = yield this._waterline.collections[resourceName].destroy({id:resourceId});
        if(!resources.length) {
          resourceNotFoundError(this); 
        }
        this.response.status = 200;
    })
    
    function resourceNotFoundError(context) {
        var errMsg = "Resource not found for collection " + resourceName + ".";
        console.log(errMsg);
        context.throw(errMsg, 404);
    }
    
    return router;
}


module.exports = GenericApi;