var request = require('co-supertest');
var should = require('should');
require('co-mocha');
var appExports = require("./../app.js");
var Waterline = appExports.Waterline;
var waterlineInjection = appExports.waterlineInjection;
var server = appExports.server;

describe("Test User CRUD Api.", function(){
   var agent;
   var waterline;
   var user = {
     name : "Koa Expert",
     age: 23,
     height: "5ft 10in"  
   };
   
    before(function *() {
        agent = request.agent(server);
        waterline = yield Waterline.init(waterlineInjection);   
    });

    beforeEach(function *(){
        yield waterline.collections.users.destroy({name: user.name});
    });

    it("Should create user successfully", function *(){
        yield agent
            .post("/users")
            .send(user)
            .expect(200)
            .end();
        var foundUser = yield waterline.collections.users.findOne({name: user.name}); 
        should.exist(foundUser);
    });

    it("Should fail to create user without provided 'name'", function *(){
        var userCopy =  (JSON.parse(JSON.stringify(user)));;
        delete userCopy.name;
        yield agent
            .post("/users")
            .send(userCopy)
            .expect(400)
            .end();
    });

    it("Should retrieve the created user successfully", function *(){
        var createdUser = yield waterline.collections.users.create(user); 
        var url = "/users/" + createdUser.id;
        yield agent
                .get(url)
                .set("Accept", "application/json")
                .expect("Content-type", /json/)
                .expect(200)
                .end();
    });
    
    it('Should update an existing user', function *(){
        var createdUser = yield waterline.collections.users.create(user); 
        var url = "/users/" + createdUser.id;
        var userCopy =  (JSON.parse(JSON.stringify(user)));;
        userCopy.age = 53;
        userCopy.height = "5ft 8in";        
        yield agent
                .put(url)
                .send(userCopy)
                .expect(204)
                .end();
                
    });
    
    it('Should delete an existing user', function *(){
        var createdUser = yield waterline.collections.users.create(user); 
        var url = "/users/" + createdUser.id;    
        yield agent
                .del(url)
                .send(createdUser)
                .expect(200)
                .end();
    });
});