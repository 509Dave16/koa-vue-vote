import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource)
import VueRouter from 'vue-router'
Vue.use(VueRouter)

var App = Vue.extend({})
import Hello from './components/Hello'
import Home from './components/Home'
import Comment from './components/Comment'
import Vote from './components/Vote'

let router = new VueRouter()

router.map({
  '/home': {
    name: 'home',
    component: Home
  },
  '/vote': {
    name: 'vote',
    component: Vote
  },
  '/comment': {
    name: 'comment',
    component: Comment
  },
  '/hello': {
    component: Hello
  }
})

router.beforeEach(function () {
  window.scrollTo(0, 0)
})

router.redirect({
  '*': 'home'
})

router.start(App, '#app')
