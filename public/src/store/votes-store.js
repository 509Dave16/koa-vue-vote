import { baseDomain } from '../config/routing'
import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource)
let votesResource = Vue.resource(baseDomain + '/votes{/id}')

class VotesStore {
  constructor () {
    this.currentVote = undefined
  }
  persist () {
    return votesResource.save(this.currentVote).then((response) => {
      this.currentVote = response.data
      return this.currentVote
    })
  }
  update (data) {
    Object.assign(this.currentVote, data)
    return votesResource.update({id: this.currentVote.id}, this.currentVote).then((response) => {
      return this.currentVote
    })
  }
}

const votesStore = new VotesStore()

export default votesStore
