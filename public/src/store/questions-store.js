import { baseDomain } from '../config/routing'
import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource)
let questionsResource = Vue.resource(baseDomain + '/questions{/id}')

class QuestionsStore {
  constructor () {
    this.questions = undefined
    this.currentVote = undefined
    this.currentQuestion = undefined
  }
  refresh () {
    return questionsResource.get({}).then((response) => {
      this.questions = response.data
      return this.questions
    })
  }
}

const questionsStore = new QuestionsStore()

export default questionsStore

