//Connections
var connections = {
    mysql: {
        adapter: "mysql",
        host: 'localhost',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'koa-test'
    }
};

//Adapters
var adapters = {
    mysql:     require('sails-mysql')
};

//Models
var requireDir = require('require-dir');
var models = requireDir('./models');

//Export the above variables
var injection = {};
injection.methods           = false;
injection.models            = models;
injection.adapters          = adapters;
injection.connections       = connections;

module.exports = injection;