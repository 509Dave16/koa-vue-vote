module.exports = {
    'model': true,
    'adp': 'mysql',
    'connection': 'mysql',
    'properties': {
        'question' : {
            'type' : 'string',
            'required' : true
        },
        'votes': {
            'collection' : 'votes',
            'via': 'question_id'
        }
    }
};