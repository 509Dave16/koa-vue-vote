module.exports = {
    'model': true,
    'adp': 'mysql',
    'connection': 'mysql',
    'properties': {
        'name' : {
            'type' : 'string',
            'required' : true
        },
        'age' : {
            'type' : 'integer'
        },
        'height' : {
            'type' : 'string'
        }
    }
};