module.exports = {
    'model': true,
    'adp': 'mysql',
    'connection': 'mysql',
    'properties': {
        'rating' : {
            'type' : 'integer',
            'required' : true
        },
        'comment' : {
            'type' : 'text'
        },
        'question_id': {
            'type': 'integer',
            'model': 'questions'
        }
    }
};