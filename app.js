var koa = require("koa");
var cors = require("koa-cors");
var app = koa();
var routes = require('./routing/routes.js');
var bodyParser = require('koa-bodyparser');
var waterlineInjection = require('./db/waterline-injection.js');
var Waterline = require('koa-waterline');
var serve = require('koa-static');

var corsOptions = {
    origin: '*',
    methods: 'OPTIONS,POST,GET,PUT,DELETE',
    headers: 'Origin,X-Requested-With,Content-Type,Accept' 
};

//Allow cross origin scripting
app.use(cors(corsOptions));
//Serve Files
app.use(serve(__dirname + '/public'));
//Parse Body
app.use(bodyParser());
//Attach Waterline to Context object
app.use(Waterline(waterlineInjection));
//Import Routes
routes(app);
//Start Server
var server = app.listen(3000);

//Export variables to be used for testing
module.exports = {
    waterlineInjection: waterlineInjection,
    Waterline: Waterline,
    server: server
}

console.log("The app is listening on Port 3000!");